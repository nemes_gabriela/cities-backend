package com.example.city.service;

import com.example.city.model.City;
import com.example.city.repository.CityRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityService implements ICityService {

    @Autowired
    private CityRepository repository;

    @Override
    public List<City> findAll() {
        List<City> cities = (List<City>)repository.findAll();
        return cities;
    }

    @Override
    public void update(City city, long id) {
        repository.findById(id)
                .map( newCity -> {
                        newCity.setName(city.getName());
                        newCity.setPopulation(city.getPopulation());
                        return repository.save(newCity);
                    });
        /*.orElseGet(() -> {
            newEmployee.setId(id);
            return repository.save(newEmployee);
        });*/
    }

    @Override
    public City findById(long id) {
        return repository.findById(id).orElseThrow(() -> new IllegalArgumentException(""));
    }

    @Override
    public City add(City city) {
        return repository.save(city);
    }

    @Override
    public void deleteById(long id) {
        repository.deleteById(id);
    }

    @Override
    public List<City> findByNameContainsIgnoreCase(String name) {
        List<City> list = repository.findByNameContainsIgnoreCase(name);
        return list;
    }
}
