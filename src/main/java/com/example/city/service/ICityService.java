package com.example.city.service;

import com.example.city.model.City;

import java.util.List;

public interface ICityService {
    List<City> findAll();

    void update(City city, long id);

    City findById(long id);

    City add(City city);

    void deleteById(long id);

    List<City> findByNameContainsIgnoreCase(String name);
}
