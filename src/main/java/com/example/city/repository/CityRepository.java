package com.example.city.repository;

import com.example.city.model.City;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends CrudRepository<City, Long>{

    List<City> findByNameContainsIgnoreCase(String name);
}
