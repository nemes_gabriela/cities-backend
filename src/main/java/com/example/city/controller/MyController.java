package com.example.city.controller;

import com.example.city.model.City;
import com.example.city.service.ICityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MyController {

    @Autowired
    private ICityService cityService;

    /*@CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/showCities")
    public String findCities(Model model) {
        List<City> cities = (List<City>) cityService.findAll();
        model.addAttribute("cities", cities);
        return "showCities";
    }*/

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/cities")
    public List<City> getCities() {
        List<City> cities = (List<City>) cityService.findAll();
        return cities;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/cities/{id}")
    public City getCities(@PathVariable long id) {
        return (City)cityService.findById(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/cities/{id}")
    public void update(@RequestBody City city, @PathVariable long id) {
        cityService.update(city, id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/cities")
    public City add(@RequestBody City city) {
        return cityService.add(city);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/cities/{id}")
    public void delete(@PathVariable long id){
        cityService.deleteById(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/cities/search")
    public List<City> search(@RequestParam String name){
        return cityService.findByNameContainsIgnoreCase(name);
    }
}
